#jobeet

## download symfony 1.4

## start jobeet
* view symfony version

```
php lib/vendor/symfony/data/bin/symfony -V
```

* create project use symfony commad

```
php lib/vendor/symfony/data/bin/symfony generate:project jobeet --orm=Propel
```
>generate:project任务生成了 symfony 项目默认的文件和目录结构：
目录	说明
apps/	存放项目的所有应用程序
cache/	框架的缓存文件
config/	项目配置文件
lib/	项目使用到的类和库
log/	项目日志文件
plugins/	安装的插件
test/	单元测试和功能测试文件
web/	网站根目录（见下文）

from new we can user ./symfony to generate code

* create app

```
./symfony symfony generate:app frontend
```

